<?php

namespace App\Modules\Practices\Migrations;

use T4\Orm\Migration;

class m_1473087741_createPractices
    extends Migration
{

    public function up()
    {
        $this->createTable('practices', [
            'title' => ['type' => 'string', 'length' => 1024],
            'url' => ['type' => 'string'],
            'image' => ['type'=>'string'],
            'sm_image' => ['type' => 'string'],
            'text' => ['type' => 'text', 'length' => 'big'],
            'video' => ['type' => 'string'],
            'description' => ['type' => 'string'],
            'keywords' => ['type' => 'string'],
            'published' => ['type' => 'bool', 'default' => 0],
        ]);
    }

    public function down()
    {
        $this->dropTable('practices');
    }

}