<?php

namespace App\Modules\Practices\Controllers;

use App\Components\Admin\Controller;
use App\Modules\Practices\Models\Practice;

class Admin
    extends Controller
{

    public function actionDefault()
    {
        $this->data->items = Practice::findAll(['order' => 'weight']);
    }

    public function actionEdit($id = null)
    {
        if (null === $id || 'new' == $id) {
            $this->data->item = new Practice();
        } else {
            $this->data->item = Practice::findByPK($id);
        }
    }

    public function actionSave()
    {
        if (!empty($this->app->request->post->id)) {
            $item = Practice::findByPK($this->app->request->post->id);
        } else {
            $item = new Practice();
        }

        if (empty($this->app->request->post->published)) {
            $item->published = 0;
        }

        $item->fill($this->app->request->post);
        $item->uploadImage('image')
             ->uploadSmallImage('sm_image')
             ->save();
        $this->redirect('/admin/practices');
    }

    public function actionDelete($id)
    {
        $item = Practice::findByPK($id);
        if ($item) {
            $item->delete();
        }
        $this->redirect('/admin/practices');
    }

    public function actionDeleteImage($id)
    {
        $item = Practice::findByPK($id);
        if ($item) {
            $item->deleteImage();
            $item->save();
            $this->data->result = true;
        } else {
            $this->data->result = false;
        }
    }

    public function actionDeleteSmallImage($id)
    {
        $item = Practice::findByPK($id);
        if ($item) {
            $item->deleteSmallImage();
            $item->save();
            $this->data->result = true;
        } else {
            $this->data->result = false;
        }
    }
}