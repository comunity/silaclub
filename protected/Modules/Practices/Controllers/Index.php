<?php

namespace App\Modules\Practices\Controllers;

use App\Modules\Practices\Models\Practice;
use T4\Http\E404Exception;
use T4\Mvc\Controller;

class Index
    extends Controller
{
    public function actionDefault()
    {
        $this->data->items = Practice::findAll(['where' => 'published = 1', 'order' => 'weight']);
    }

    public function actionPracticeByUrl($url)
    {
        $practice = Practice::findByUrl($url);
        if (empty($practice) || empty($practice->published))
            throw new E404Exception;
        $this->data->practice = $practice;
    }

    /*public function actionPracticeById($id)
    {
        $practice = Practice::findByPK($id);
        if (empty($practice))
            throw new E404Exception;
        $this->data->practice = $practice;
    }*/

}