<?php

namespace App\Modules\Practices;

class Module
    extends \App\Components\Module
{

    public function getAdminMenu()
    {
        return [
                ['title' => 'Практики','icon' => '<i class="glyphicon glyphicon-list"></i>', 'url' => '/admin/practices/'],
        ];
    }

} 