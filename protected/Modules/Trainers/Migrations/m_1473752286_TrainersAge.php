<?php

namespace App\Modules\Trainers\Migrations;

use T4\Orm\Migration;

class m_1473752286_TrainersAge
    extends Migration
{

    public function up()
    {
        $this->dropColumn('trainers', ['age']);
        $this->addColumn('trainers', [
            'birthday' => ['type' => 'date']
        ]);
    }

    public function down()
    {
        $this->addColumn('trainers', [
            'age' => ['type' => 'tiny']
        ]);
        $this->dropColumn('trainers', ['age']);
    }

}