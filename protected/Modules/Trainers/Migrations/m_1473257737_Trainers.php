<?php

namespace App\Modules\Trainers\Migrations;

use T4\Orm\Migration;

class m_1473257737_Trainers
    extends Migration
{

    public function up()
    {
        $this->createTable('trainers', [
            'firstName' => ['type' => 'string', 'length' => 50],
            'lastName' => ['type' => 'string', 'length' => 50],
            'patronymic' => ['type' => 'string', 'lenght' => 50],
            'age' => ['type' => 'tiny'],
            'email' => ['type' => 'string'],
            'phone' => ['type' => 'string'],
            'text' => ['type' => 'text'],
            'photo' => ['type' => 'string'],
            'sm_photo' => ['type' => 'string'],
            'video' => ['type' => 'string'],
            'url' => ['type' => 'string'],
            'description' => ['type' => 'string'],
            'keywords' => ['type' => 'string'],
            'published' => ['type' => 'bool', 'default' => 0],
        ]);
    }

    public function down()
    {
        $this->dropTable('trainers');
    }

}