<?php

namespace App\Modules\Trainers\Controllers;

use App\Components\Admin\Controller;
use App\Modules\Practices\Models\Practice;
use App\Modules\Trainers\Models\Trainer;

class Admin
    extends Controller
{

    public function actionDefault()
    {
        $this->data->items = Trainer::findAll(['order' => 'weight']);
    }

    public function actionEdit($id = null)
    {
        if (null === $id || 'new' == $id) {
            $this->data->item = new Trainer();
        } else {
            $this->data->item = Trainer::findByPK($id);
        }
        $this->data->practices = Practice::findAll();
    }

    public function actionSave()
    {
        if (!empty($this->app->request->post->id)) {
            $item = Trainer::findByPK($this->app->request->post->id);
        } else {
            $item = new Trainer();
        }

        if (empty($this->app->request->post->published)) {
            $item->published = 0;
        }

        $item->fill($this->app->request->post);
        $item->uploadImage('photo')
             ->uploadSmallImage('sm_photo')
             ->fillPractices('practices')
             ->save();
        $this->redirect('/admin/trainers');
    }

    public function actionDelete($id)
    {
        $item = Trainer::findByPK($id);
        if ($item) {
            $item->delete();
        }
        $this->redirect('/admin/trainers');
    }

    public function actionDeleteImage($id)
    {
        $item = Trainer::findByPK($id);
        if ($item) {
            $item->deleteImage();
            $item->save();
            $this->data->result = true;
        } else {
            $this->data->result = false;
        }
    }

    public function actionDeleteSmallImage($id)
    {
        $item = Trainer::findByPK($id);
        if ($item) {
            $item->deleteSmallImage();
            $item->save();
            $this->data->result = true;
        } else {
            $this->data->result = false;
        }
    }
}