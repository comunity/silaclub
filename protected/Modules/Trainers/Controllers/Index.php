<?php

namespace App\Modules\Trainers\Controllers;

use App\Modules\Trainers\Models\Trainer;
use T4\Http\E404Exception;
use T4\Mvc\Controller;

class Index
    extends Controller
{
    public function actionDefault()
    {
        $this->data->items = Trainer::findAll(['where' => 'published = 1', 'order' => 'weight']);
    }

    public function actionTrainerByUrl($url)
    {
        $trainer = Trainer::findByUrl($url);
        if (empty($trainer) || empty($trainer->published))
            throw new E404Exception;
        $this->data->trainer = $trainer;
    }

    /*public function actionPracticeById($id)
    {
        $practice = Practice::findByPK($id);
        if (empty($practice))
            throw new E404Exception;
        $this->data->practice = $practice;
    }*/

}