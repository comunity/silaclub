<?php

namespace App\Modules\Trainers;

class Module
    extends \App\Components\Module
{

    public function getAdminMenu()
    {
        return [
                ['title' => 'Тренера','icon' => '<i class="glyphicon glyphicon-user"></i>', 'url' => '/admin/trainers/'],
        ];
    }

} 