<?php

namespace App\Modules\Trainers\Models;

use App\Modules\Practices\Models\Practice;
use T4\Core\Collection;
use T4\Core\Exception;
use T4\Fs\Helpers;
use T4\Http\Uploader;
use T4\Mvc\Application;
use T4\Orm\Model;

class Trainer
    extends Model
{
    static protected $schema = [
        'table' => 'trainers',
        'columns' => [
            'firstName' => ['type' => 'string', 'length' => 50],
            'lastName' => ['type' => 'string', 'length' => 50],
            'patronymic' => ['type' => 'string', 'lenght' => 50],
            'email' => ['type' => 'string'],
            'phone' => ['type' => 'string'],
            'text' => ['type' => 'text'],
            'photo' => ['type' => 'string'],
            'sm_photo' => ['type' => 'string'],
            'video' => ['type' => 'string'],
            'url' => ['type' => 'string'],
            'description' => ['type' => 'string'],
            'keywords' => ['type' => 'string'],
            'published' => ['type' => 'bool', 'default' => 0],
            'birthday' => ['type' => 'date'],
            'weight' => ['type' => 'int', 'default' => 0],
        ],
        'relations' => [
            'practices' => ['type' => self::MANY_TO_MANY, 'model' => Practice::class]
        ],
    ];

    public function fillPractices($formFieldName)
    {
        $request = Application::getInstance()->request;
        $i = new Collection();
        if (isset($request->post->$formFieldName)) {
            $items = array_unique($request->post->$formFieldName->toArray());
            foreach ($items as $id) {
                $item = Practice::findByPK($id);
                if (!empty($item)) {
                    $i[] = $item;
                }
            }
        }
        $this->setPractices($i);
        return $this;
    }

    public function getFullName()
    {
        return $this->lastName . ' ' . $this->firstName . ' ' . $this->patronymic;
    }

    public function getAge()
    {
        $date = new \DateTime();
        $birthday = new \DateTime($this->birthday);
        if (checkdate($birthday->format('n'), $birthday->format('j'), $birthday->format('Y'))) {
            $age = $date->diff($birthday)->format('%y');
        } else {
            $age = null;
        }
        return $age;
    }

    public function uploadImage($formFieldName)
    {
        $request = Application::getInstance()->request;
        if (!$request->existsFilesData() || !$request->isUploaded($formFieldName) || $request->isUploadedArray($formFieldName))
            return $this;

        try {
            $uploader = new Uploader($formFieldName);
            $uploader->setPath('/img/trainers/big');
            $image = $uploader();
            if ($this->photo)
                $this->deleteImage();
            $this->photo = $image;
        } catch (Exception $e) {
            $this->photo = null;
        }
        return $this;
    }

    public function uploadSmallImage($formFieldName)
    {
        $request = Application::getInstance()->request;
        if (!$request->existsFilesData() || !$request->isUploaded($formFieldName) || $request->isUploadedArray($formFieldName))
            return $this;

        try {
            $uploader = new Uploader($formFieldName);
            $uploader->setPath('/img/trainers/small');
            $image = $uploader();
            if ($this->sm_photo)
                $this->deleteSmallImage();
            $this->sm_photo = $image;
        } catch (Exception $e) {
            $this->sm_photo = null;
        }
        return $this;
    }

    public function beforeDelete()
    {
        $this->deleteImage();
        $this->deleteSmallImage();
        return parent::beforeDelete();
    }

    public function deleteImage()
    {
        if ($this->photo) {
            try {
                Helpers::removeFile(ROOT_PATH_PUBLIC . $this->photo);
                $this->photo = '';
            } catch (\T4\Fs\Exception $e) {
                return false;
            }
        }
        return true;
    }

    public function deleteSmallImage()
    {
        if ($this->sm_photo) {
            try {
                Helpers::removeFile(ROOT_PATH_PUBLIC . $this->sm_photo);
                $this->sm_photo = '';
            } catch (\T4\Fs\Exception $e) {
                return false;
            }
        }
        return true;
    }
}