<?php

namespace App\Modules\Dishes;

class Module
    extends \App\Components\Module
{

    public function getAdminMenu()
    {
        return [
            ['title' => 'Кафе', 'icon' => '<i class="glyphicon glyphicon-cutlery"></i>', 'sub' => [
                ['title' => 'Блюда', 'url' => '/admin/dishes/'],
                ['title' => 'Категории блюд', 'url' => '/admin/dishes/categories/'],
            ]],
        ];
    }

} 