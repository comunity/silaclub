<?php

namespace App\Modules\Dishes\Controllers;

use App\Components\Admin\Controller;
use App\Modules\Dishes\Models\Category;
use App\Modules\Dishes\Models\Dish;

class Admin
    extends Controller
{
    protected static function getDishesByDate($date)
    {
        $dishes = Dish::findAll(['where' => 'cookingdate = :date',
                                 'params' => [':date' => $date],
                                 'order' => '__category_id']
        );
        return $dishes;
    }

    public function actionDefault($date = null)
    {
        if (empty($date)) {
            $date = date('Y-m-d');
        }

        $this->data->items = self::getDishesByDate($date);
        $this->data->currentDate = $date;
    }

    public function actionEdit($id = null, $date = null)
    {
        if (null === $id || 'new' == $id) {
            $this->data->item = new Dish();
            $this->data->item->cookingdate = $date;
        } else {
            $this->data->item = Dish::findByPK($id);
        }
        $this->data->categories = Category::findAll();
    }

    public function actionSave()
    {
        $id = $this->app->request->post->id;
        if (!empty($id)) {
            $item = Dish::findByPK($id);
        } else {
            $item = new Dish();
        }
        $item->fill($this->app->request->post);
        $item->save();
        $this->redirect('/admin/dishes?date=' . date_format(date_create($item->cookingdate), 'Y-m-d'));
    }

    public function actionDelete($id)
    {
        $item = Dish::findByPK($id);
        if ($item) {
            $item->delete();
        }
        $this->redirect('/admin/dishes?date=' . date_format(date_create($item->cookingdate), 'Y-m-d'));
    }

    public function actionGetDishesByDate($date = null)
    {
        if (empty($date)) {
            $date = date('Y-m-d');
        }

        $this->data->items = self::getDishesByDate($date);
        $this->data->currentDate = $date;
    }

    public function actionGetDishesToCopy($copyDate, $date)
    {
        if (empty($copyDate)) {
            $copyDate = date('Y-m-d');
        }

        $this->data->items = self::getDishesByDate($copyDate);
        $this->data->currentDate = $date;
    }

    public function actionCopy($date)
    {
        if (!empty($this->app->request->post->items)) {
            foreach ($this->app->request->post->items as $item) {
                if (!empty($item->copy) && 1 == $item->copy) {
                    $dish = Dish::findByPK($item->id);
                    $copyDish = clone $dish;
                    $copyDish->cookingdate = $date;
                    $copyDish->save();
                }
            }
        }

        $this->redirect('/admin/dishes?date=' . $date);
    }

    /**
     * CATEGORIES
     */

    public function actionCategories()
    {
        $this->data->items = Category::findAll();
    }

    public function actionCategoryEdit($id = null)
    {
        if (null === $id || 'new' == $id) {
            $this->data->item = new Category();
        } else {
            $this->data->item = Category::findByPK($id);
        }
    }

    public function actionCategorySave() {
        if (!empty($this->app->request->post->id)) {
            $item = Category::findByPK($this->app->request->post->id);
        } else {
            $item = new Category();
        }
        $item->fill($this->app->request->post);
        $item->save();
        $this->redirect('/admin/dishes/categories/');
    }

    public function actionCategoryDelete($id)
    {
        $item = Category::findByPK($id);
        if ($item) {
            $item->delete();
        }
        $this->redirect('/admin/dishes/categories');
    }

    public function actionPrintMenu($date = null)
    {
        if (empty($date)) {
            $date = date('Y-m-d');
        }
        $dishes = self::getDishesByDate($date);
        $dishes = $dishes->group('__category_id');
        $this->data->items = $dishes;
        $this->data->currentDate = $date;
    }
}