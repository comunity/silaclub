<?php

namespace App\Modules\Dishes\Models;

use T4\Orm\Model;

class Dish
    extends Model
{
    protected static $schema = [
        'table' => 'dishes',
        'columns' => [
            'title' => ['type' => 'string'],
            'comment' => ['type' => 'text'],
            'cookingdate' => ['type' => 'date'],
        ],
        'relations' => [
            'category' => ['type' => self::BELONGS_TO, 'model' => Category::class]
        ]
    ];

    public function __clone()
    {
        $this->__id = null;
        $this->isNew = true;
    }
} 