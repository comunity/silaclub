<?php

namespace App\Modules\Dishes\Models;

use T4\Orm\Model;

class Category
    extends Model
{
    protected static $schema = [
        'table' => 'categories',
        'columns' => [
            'title' => ['type' => 'string'],
            'desc' => ['type' => 'text'],
        ]
    ];
}