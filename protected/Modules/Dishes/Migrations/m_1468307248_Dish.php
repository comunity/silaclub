<?php

namespace App\Modules\Dishes\Migrations;

use T4\Orm\Migration;

class m_1468307248_Dish
    extends Migration
{

    public function up()
    {
        $this->createTable('dishes', [
            'title' => ['type' => 'string'],
            'comment' => ['type' => 'text'],
            'cookingdate' => ['type' => 'date'],
            '__category_id' => ['type' => 'link']
        ]);
    }

    public function down()
    {
        $this->dropTable('dishes');
    }

}