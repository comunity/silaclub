<?php

namespace App\Modules\Dishes\Migrations;

use T4\Orm\Migration;

class m_1468306839_Category
    extends Migration
{

    public function up()
    {
        $this->createTable('categories', [
            'title' => ['type' => 'string'],
            'desc' => ['type' => 'text'],
        ]);
    }

    public function down()
    {
        $this->dropTable('categories');
    }

}