<?php

namespace App\Modules\Pages\Migrations;

use T4\Orm\Migration;

class m_1489754516_addColumnPublishedPage
    extends Migration
{

    public function up()
    {
        $this->addColumn('pages', [
            'published' => ['type' => 'bool', 'default' => 0]
        ]);
    }

    public function down()
    {
        $this->dropColumn('pages', ['published']);
    }

}