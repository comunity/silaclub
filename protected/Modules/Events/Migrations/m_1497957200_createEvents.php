<?php

namespace App\Modules\Events\Migrations;

use T4\Orm\Migration;

class m_1497957200_createEvents
    extends Migration
{

    public function up()
    {
        $this->createTable('events', [
            'title' => ['type' => 'string', 'length' => 1024],
            'url' => ['type' => 'string'],
            'rundate' => ['type' => 'date'],
            'image' => ['type' => 'string'],
            'sm_image' => ['type' => 'string'],
            'text' => ['type' => 'text', 'length' => 'big'],
            'video' => ['type' => 'string'],
            'description' => ['type' => 'string'],
            'keywords' => ['type' => 'string'],
            'published' => ['type' => 'bool', 'default' => 0],
        ]);
    }

    public function down()
    {
        $this->dropTable('events');
    }

}