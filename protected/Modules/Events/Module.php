<?php

namespace App\Modules\Events;

class Module
    extends \App\Components\Module
{

    public function getAdminMenu()
    {
        return [
                ['title' => 'События','icon' => '<i class="glyphicon glyphicon-bell"></i>', 'url' => '/admin/events/'],
        ];
    }

} 