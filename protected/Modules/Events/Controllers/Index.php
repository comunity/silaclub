<?php

namespace App\Modules\Events\Controllers;

use App\Modules\Events\Models\Event;
use T4\Http\E404Exception;
use T4\Mvc\Controller;

class Index
    extends Controller
{
    const PAGE_SIZE = 30;

    public function actionDefault($page = 1)
    {
        $this->data->itemsCount = Event::countAll(['where' => 'published = 1 AND rundate < CURDATE()']);
        $this->data->pageSize = self::PAGE_SIZE;
        $this->data->activePage = $page;

        if (1 == $page) {
            $this->data->upcomingEvents = Event::findAll([
                'where' => 'published = 1 AND rundate >=  CURDATE()',
                'order' => 'rundate'
            ]);
        }

        $this->data->pastEvents = Event::findAll([
            'where' => 'published = 1 AND rundate <  CURDATE()',
            'order' => 'rundate DESC',
            'offset' => ($page-1)*self::PAGE_SIZE,
            'limit' => self::PAGE_SIZE
        ]);
    }

    public function actionEventByUrl($url)
    {
        $event = Event::findByUrl($url);
        if (empty($event) || empty($event->published))
            throw new E404Exception;
        $this->data->event = $event;
    }

    /*public function actionEventById($id)
    {
        $event = Event::findByPK($id);
        if (empty($event))
            throw new E404Exception;
        $this->data->event = $event;
    }*/

}