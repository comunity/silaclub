<?php

namespace App\Modules\Events\Controllers;

use App\Components\Admin\Controller;
use App\Modules\Events\Models\Event;

class Admin
    extends Controller
{

    public function actionDefault()
    {
        $this->data->items = Event::findAll(['order' => 'rundate DESC']);
    }

    public function actionEdit($id = null)
    {
        if (null === $id || 'new' == $id) {
            $this->data->item = new Event();
        } else {
            $this->data->item = Event::findByPK($id);
        }
    }

    public function actionSave()
    {
        if (!empty($this->app->request->post->id)) {
            $item = Event::findByPK($this->app->request->post->id);
        } else {
            $item = new Event();
        }

        if (empty($this->app->request->post->published)) {
            $item->published = 0;
        }

        $item->fill($this->app->request->post);
        $item->uploadImage('image')
             ->uploadSmallImage('sm_image')
             ->save();
        $this->redirect('/admin/events');
    }

    public function actionDelete($id)
    {
        $item = Event::findByPK($id);
        if ($item) {
            $item->delete();
        }
        $this->redirect('/admin/events');
    }

    public function actionDeleteImage($id)
    {
        $item = Event::findByPK($id);
        if ($item) {
            $item->deleteImage();
            $item->save();
            $this->data->result = true;
        } else {
            $this->data->result = false;
        }
    }

    public function actionDeleteSmallImage($id)
    {
        $item = Event::findByPK($id);
        if ($item) {
            $item->deleteSmallImage();
            $item->save();
            $this->data->result = true;
        } else {
            $this->data->result = false;
        }
    }
}