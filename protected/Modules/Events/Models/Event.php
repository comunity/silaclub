<?php

namespace App\Modules\Events\Models;

use T4\Core\Exception;
use T4\Fs\Helpers;
use T4\Http\Uploader;
use T4\Mvc\Application;
use T4\Orm\Model;

class Event
    extends Model
{
    static protected $schema = [
        'table' => 'events',
        'columns' => [
            'title' => ['type' => 'string'],
            'url' => ['type' => 'string'],
            'rundate' => ['type' => 'date'],
            'image' => ['type'=>'string', 'default'=>''],
            'sm_image' => ['type'=>'string', 'default'=>''],
            'text' => ['type' => 'text'],
            'video' => ['type' => 'string'],
            'description' => ['type' => 'string'],
            'keywords' => ['type' => 'string'],
            'published' => ['type' => 'bool', 'default' => 0],
        ]
    ];

    public function uploadImage($formFieldName)
    {
        $request = Application::getInstance()->request;
        if (!$request->existsFilesData() || !$request->isUploaded($formFieldName) || $request->isUploadedArray($formFieldName))
            return $this;

        try {
            $uploader = new Uploader($formFieldName);
            $uploader->setPath('/img/events/big');
            $image = $uploader();
            if ($this->image)
                $this->deleteImage();
            $this->image = $image;
        } catch (Exception $e) {
            $this->image = null;
        }
        return $this;
    }

    public function uploadSmallImage($formFieldName)
    {
        $request = Application::getInstance()->request;
        if (!$request->existsFilesData() || !$request->isUploaded($formFieldName) || $request->isUploadedArray($formFieldName))
            return $this;

        try {
            $uploader = new Uploader($formFieldName);
            $uploader->setPath('/img/events/small');
            $image = $uploader();
            if ($this->sm_image)
                $this->deleteSmallImage();
            $this->sm_image = $image;
        } catch (Exception $e) {
            $this->sm_image = null;
        }
        return $this;
    }

    public function beforeDelete()
    {
        $this->deleteImage();
        $this->deleteSmallImage();
        return parent::beforeDelete();
    }

    public function deleteImage()
    {
        if ($this->image) {
            try {
                Helpers::removeFile(ROOT_PATH_PUBLIC . $this->image);
                $this->image = '';
            } catch (\T4\Fs\Exception $e) {
                return false;
            }
        }
        return true;
    }

    public function deleteSmallImage()
    {
        if ($this->sm_image) {
            try {
                Helpers::removeFile(ROOT_PATH_PUBLIC . $this->sm_image);
                $this->sm_image = '';
            } catch (\T4\Fs\Exception $e) {
                return false;
            }
        }
        return true;
    }
}