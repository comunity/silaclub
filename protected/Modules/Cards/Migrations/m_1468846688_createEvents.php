<?php

namespace App\Modules\Cards\Migrations;

use T4\Orm\Migration;

class m_1468846688_createEvents
    extends Migration
{

    public function up()
    {
        $this->createTable('events', [
            'title' => ['type' => 'string'],
            'desc' => ['type' => 'string'],
            'startdate' => ['type' => 'string'],
        ]);
    }

    public function down()
    {
        $this->dropTable('events');
    }

}