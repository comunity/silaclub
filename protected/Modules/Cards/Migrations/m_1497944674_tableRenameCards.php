<?php

namespace App\Modules\Cards\Migrations;

use T4\Orm\Migration;

class m_1497944674_tableRenameCards
    extends Migration
{

    public function up()
    {
        $this->renameTable('events', 'cards');
    }

    public function down()
    {
        $this->renameTable('cards', 'events');
    }

}