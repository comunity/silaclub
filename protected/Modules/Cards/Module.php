<?php

namespace App\Modules\Cards;

class Module
    extends \App\Components\Module
{

    public function getAdminMenu()
    {
        return [
            ['title' => 'Мероприятия', 'icon' => '<i class="glyphicon glyphicon-star"></i>', 'url' => '/admin/cards/'],
        ];
    }

} 