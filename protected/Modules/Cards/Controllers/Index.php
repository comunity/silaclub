<?php

namespace App\Modules\Cards\Controllers;

use App\Modules\Cards\Models\Card;
use T4\Http\E404Exception;
use T4\Mvc\Controller;

class Index
    extends Controller
{
    public function actionOneCard($id)
    {
        $this->data->item = Card::findByPK($id);
    }
} 