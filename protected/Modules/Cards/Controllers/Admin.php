<?php

namespace App\Modules\Cards\Controllers;

use App\Components\Admin\Controller;
use App\Modules\Cards\Models\Card;

class Admin
    extends Controller
{

    public function actionDefault()
    {
        $this->data->items = Card::findAll();
    }

    public function actionEdit($id = null)
    {
        if (null === $id || 'new' == $id) {
            $this->data->item = new Card();
        } else {
            $this->data->item = Card::findByPK($id);
        }
    }

    public function actionSave()
    {
        $id = $this->app->request->post->id;
        if (!empty($id)) {
            $item = Card::findByPK($id);
        } else {
            $item = new Card();
        }
        $item->fill($this->app->request->post);
        $item->save();
        $this->redirect('/admin/Cards');
    }

    public function actionDelete($id)
    {
        $item = Card::findByPK($id);
        if ($item) {
            $item->delete();
        }
        $this->redirect('/admin/Cards');
    }
}