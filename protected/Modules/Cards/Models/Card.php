<?php

namespace App\Modules\Cards\Models;

use T4\Orm\Model;

class Card
    extends Model
{
    protected static $schema = [
        'table' => 'cards',
        'columns' => [
            'title' => ['type' => 'string'],
            'desc' => ['type' => 'string'],
            'startdate' => ['type' => 'string'],
        ],
    ];
}