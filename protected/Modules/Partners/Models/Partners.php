<?php

namespace App\Modules\Partners\Models;

use T4\Core\Exception;
use T4\Fs\Helpers;
use T4\Http\Uploader;
use T4\Mvc\Application;
use T4\Orm\Model;

class Partners
    extends Model
{
    static protected $schema = [
        'table' => 'partners',
        'columns' => [
            'url' => ['type'=>'string'],
            'logo' => ['type'=>'string', 'default'=>''],
            'text' => ['type'=>'text'],
            'published' => ['type' => 'bool', 'default' => 0],
            'weight' => ['type' => 'int', 'default' => 0],
            'title' => ['type' => 'string'],
        ],
    ];

    public function uploadImage($formFieldName)
    {
        $request = Application::getInstance()->request;
        if (!$request->existsFilesData() || !$request->isUploaded($formFieldName) || $request->isUploadedArray($formFieldName))
            return $this;

        try {
            $uploader = new Uploader($formFieldName);
            $uploader->setPath('/img/partners');
            $image = $uploader();
            if ($this->logo)
                $this->deleteImage();
            $this->logo = $image;
        } catch (Exception $e) {
            $this->logo = null;
        }
        return $this;
    }

    public function beforeDelete()
    {
        $this->deleteImage();
        return parent::beforeDelete();
    }

    public function deleteImage()
    {
        if ($this->logo) {
            try {
                Helpers::removeFile(ROOT_PATH_PUBLIC . $this->logo);
                $this->logo = '';
            } catch (\T4\Fs\Exception $e) {
                return false;
            }
        }
        return true;
    }

}