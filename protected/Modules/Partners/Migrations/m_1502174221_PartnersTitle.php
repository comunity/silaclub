<?php

namespace App\Modules\Partners\Migrations;

use T4\Orm\Migration;

class m_1502174221_PartnersTitle
    extends Migration
{

    public function up()
    {
        $this->addColumn('partners', [
            'title' => ['type' => 'string']
        ]);
    }

    public function down()
    {
        $this->dropColumn('partners', ['title']);
    }

}