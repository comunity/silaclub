<?php

namespace App\Modules\Partners\Migrations;

use T4\Orm\Migration;

class m_1501582603_Create_Partners
    extends Migration
{

    public function up()
    {
        $this->createTable('partners', [
            'url' => ['type' => 'string'],
            'logo' => ['type' => 'string'],
            'text' => ['type' => 'text'],
            'published' => ['type' => 'bool', 'default' => 0],
            'weight' => ['type' => 'int', 'default' => 0],
        ]);
    }

    public function down()
    {
        $this->dropTable('partners');
    }

}