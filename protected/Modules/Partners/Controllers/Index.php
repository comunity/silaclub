<?php

namespace App\Modules\Partners\Controllers;

use App\Modules\Partners\Models\Partners;
use T4\Http\E404Exception;
use T4\Mvc\Controller;

class Index
    extends Controller
{

    public function actionDefault()
    {
        $this->data->items = Partners::findAll(['where' => 'published = 1', 'order' => 'weight']);
    }

}