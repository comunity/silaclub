<?php

namespace App\Modules\Partners\Controllers;

use App\Components\Admin\Controller;
use App\Modules\Partners\Models\Partners;
use T4\Core\Exception;


class Admin
    extends Controller
{

    public function actionDefault()
    {
       $this->data->items = Partners::findAll(['order' => 'weight']);
    }

    public function actionEdit($id = null)
    {
        if (null === $id || 'new' == $id) {
            $this->data->item = new Partners();
        } else {
            $this->data->item = Partners::findByPK($id);
        }
    }

    public function actionSave()
    {
        if (!empty($this->app->request->post->id)) {
            $item = Partners::findByPK($this->app->request->post->id);
        } else {
            $item = new Partners();
        }

        if (empty($this->app->request->post->published)) {
            $item->published = 0;
        }

        $item->fill($this->app->request->post);
        $item
            ->uploadImage('logo')
            ->save();
        $this->redirect('/admin/partners/');
    }

    public function actionDelete($id)
    {
        $item = Partners::findByPK($id);
        $item->delete();
        $this->redirect('/admin/partners/');
    }

    public function actionDeleteImage($id)
    {
        $item = Partners::findByPK($id);
        if ($item) {
            $item->deleteImage();
            $item->save();
            $this->data->result = true;
        } else {
            $this->data->result = false;
        }
    }

}