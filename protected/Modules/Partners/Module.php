<?php

namespace App\Modules\Partners;

class Module
    extends \App\Components\Module
{

    public function getAdminMenu()
    {
        return [
            ['title' => 'Партнеры', 'icon' => '<i class="glyphicon glyphicon-paperclip"></i>', 'url' => '/admin/partners'],
        ];
    }

} 