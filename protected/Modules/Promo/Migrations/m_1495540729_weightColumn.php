<?php

namespace App\Modules\Promo\Migrations;

use T4\Orm\Migration;

class m_1495540729_weightColumn
    extends Migration
{

    public function up()
    {
        $this->addColumn('promotions', [
            'weight' => ['type' => 'int', 'default' => 0],
        ]);
    }

    public function down()
    {
        $this->dropColumn('promotions', ['weight']);
    }

}