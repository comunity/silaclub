<?php

namespace App\Modules\Promo\Migrations;

use T4\Orm\Migration;

class m_1494589917_promo
    extends Migration
{

    public function up()
    {
        $this->createTable('promotions', [
            'title' => ['type' => 'string'],
            'image' => ['type' => 'string'],
            'text' => ['type' => 'text'],
            'published' => ['type' => 'bool', 'default' => 0],
        ]);
    }

    public function down()
    {
        $this->dropTable('promotions');
    }

}