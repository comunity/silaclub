<?php

namespace App\Modules\Promo;

class Module
    extends \App\Components\Module
{

    public function getAdminMenu()
    {
        return [
            ['title' => 'Акции', 'icon' => '<i class="glyphicon glyphicon-gift"></i>', 'url' => '/admin/promo'],
        ];
    }

} 