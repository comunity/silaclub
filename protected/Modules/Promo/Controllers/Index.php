<?php

namespace App\Modules\Promo\Controllers;

use App\Modules\Promo\Models\Promo;
use T4\Http\E404Exception;
use T4\Mvc\Controller;

class Index
    extends Controller
{

    public function actionDefault()
    {
        $this->data->items = Promo::findAll(['where' => 'published = 1', 'order' => 'weight']);
    }

}