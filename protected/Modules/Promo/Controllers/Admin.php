<?php

namespace App\Modules\Promo\Controllers;

use App\Components\Admin\Controller;
use App\Modules\Promo\Models\Promo;
use T4\Core\Exception;


class Admin
    extends Controller
{

    public function actionDefault()
    {
       $this->data->items = Promo::findAll(['order' => 'weight']);
    }

    public function actionEdit($id = null)
    {
        if (null === $id || 'new' == $id) {
            $this->data->item = new Promo();
        } else {
            $this->data->item = Promo::findByPK($id);
        }
    }

    public function actionSave()
    {
        if (!empty($this->app->request->post->id)) {
            $item = Promo::findByPK($this->app->request->post->id);
        } else {
            $item = new Promo();
        }

        if (empty($this->app->request->post->published)) {
            $item->published = 0;
        }

        $item->fill($this->app->request->post);
        $item
            ->uploadImage('image')
            ->save();
        $this->redirect('/admin/promo/');
    }

    public function actionDelete($id)
    {
        $item = Promo::findByPK($id);
        $item->delete();
        $this->redirect('/admin/promo/');
    }

    public function actionDeleteImage($id)
    {
        $item = Promo::findByPK($id);
        if ($item) {
            $item->deleteImage();
            $item->save();
            $this->data->result = true;
        } else {
            $this->data->result = false;
        }
    }

}