<?php

namespace App\Modules\Contact\Models;

use App\Models\Entry;
use App\Models\User;
use T4\Core\MultiException;
use T4\Orm\Model;

class Contact
    extends Model
{
    static protected $schema = [
        'columns' => [
            'q_datetime' => ['type' => 'datetime'],
            'email' => ['type' => 'string'],
            'name' => ['type' => 'string'],
            'phone' => ['type' => 'string'],
            'status' => ['type' => 'boolean'],
            'fromsocnet' => ['type' => 'boolean'],
        ],
        'relations' => [
            'entry' => ['type' => self::BELONGS_TO, 'model' => Entry::class]
        ]
    ];

    public function beforeSave()
    {
        if ($this->isNew()) {
            $this->q_datetime = date('Y-m-d H:i:s', time());
        }

        if (11 == strlen($this->phone)) {
            $this->phone = substr($this->phone, 1);
        }

        return true;
    }

    public function validate()
    {
        $errors = new MultiException();

        if (empty($this->phone)) {
            $errors->add('Поле "Телефон" не может быть пустым');
        }

        if (strlen($this->phone) < 10 || strlen($this->phone) > 11) {
            $errors->add('Номер телефона должен содержать 10 или 11 цифр');
        }

        if (empty($this->name)) {
            $errors->add('Поле "Имя" не может быть пустым');
        }

        if (!empty($this->entry->status) && (2 == $this->entry->status->getPk() || 3 == $this->entry->status->getPk() || 4 == $this->entry->status->getPk())) {
            $errors->add($this->entry->status->title);
        }

        if (time() >= strtotime($this->entry->timeEnd)) {
            $errors->add('Занятие уже закончилось');
        }

        if (!$this->entry->published) {
            $errors->add('Занятие закрыто');
        }

        if (!$errors->isEmpty()) {
            throw $errors;
        }

        return true;
    }

    protected function sanitizeName($value) {
        return trim($value);
    }

    protected function sanitizePhone($value) {
        return preg_replace('~\D+~', '', $value);
    }
}
