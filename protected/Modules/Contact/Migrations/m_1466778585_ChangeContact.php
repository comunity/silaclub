<?php

namespace App\Modules\Contact\Migrations;

use T4\Orm\Migration;

class m_1466778585_ChangeContact
    extends Migration
{

    public function up()
    {
        $this->dropColumn('contacts', ['question', 'a_datetime', 'answer', '__user_id']);

        $this->addColumn('contacts', [
            '__entry_id' => ['type' => 'link'],
            'phone' => ['type' => 'string'],
            'status' => ['type' => 'boolean']
        ]);
    }

    public function down()
    {
        $this->addColumn('contacts', [
            'q_datetime' => ['type' => 'datetime'],
            'email' => ['type' => 'string'],
            'name' => ['type' => 'string'],
            'question' => ['type' => 'text'],
            'a_datetime' => ['type' => 'datetime'],
            'answer' => ['type' => 'text'],
            '__user_id' => ['type' => 'link'],
        ]);

        $this->dropColumn('contacts', ['__entry_id', 'phone', 'status']);
    }

}