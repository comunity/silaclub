<?php

namespace App\Modules\Contact\Migrations;

use T4\Orm\Migration;

class m_1474447128_ContactSocial
    extends Migration
{

    public function up()
    {
        $this->addColumn('contacts', [
            'fromsocnet' => ['type' => 'boolean'],
        ]);
    }

    public function down()
    {
        $this->dropColumn('contacts', ['fromsocnet']);
    }

}