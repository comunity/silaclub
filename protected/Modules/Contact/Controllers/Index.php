<?php
namespace App\Modules\Contact\Controllers;
use App\Models\Entry;
use App\Models\User;
use App\Modules\Contact\Models\Contact;
use T4\Core\MultiException;
use T4\Core\Session;
use T4\Mail\Sender;
use T4\Mvc\Controller;
class Index
    extends Controller
{
    const ERROR_INVALID_EMAIL = 100;
    const ERROR_INVALID_CAPTCHA = 102;


    public function actionSend()
    {
        $this->data->phone = $this->app->config->phone;

        if (!empty($this->app->request->post->entry)) {
            $msg = new Contact();
            $fromsocnet = '';
            if (Session::get('social')) {
                $msg->fromsocnet = true;
                $fromsocnet = 'Перешли из социальных сетей';
            }
            $msg->fill($this->app->request->post);
            try {
                $msg->save();
            } catch (MultiException $e) {
                $errors = [];
                foreach ($e as $item) {
                    $errors[] = $item->getMessage();
                }
                $this->data->result = false;
                $this->data->errors = $errors;
                return;
            }

            $dateStart = new \DateTime($msg->entry->timeStart);
            $dateEnd = new \DateTime($msg->entry->timeEnd);
            $lessonTitle = $msg->entry->lesson ? $msg->entry->lesson->title : '';
            $teacherFirstName = $msg->entry->teacher ? $msg->entry->teacher->firstName : '';
            $teacherLastName = $msg->entry->teacher ? $msg->entry->teacher->lastName : '';
            $roomTitle = $msg->entry->room ? $msg->entry->room->title : '';

            $managerContent = <<<MSG
<h2>Заявка на занятие: {$lessonTitle}</h2>
<p>Имя: {$msg->name}</p>
<p>Телефон: {$msg->phone}</p>
<p>Почта: {$msg->email}</p>
<p>Время занятия: {$dateStart->format('d-m-Y')} с {$dateStart->format('H:i')} до {$dateEnd->format('H:i')}</p>
<p>Инструктор: {$teacherFirstName} {$teacherLastName}</p>
<p>Зал: {$roomTitle}</p>
<p>{$fromsocnet}</p>
MSG;

            $senders = ['info@silaclub.ru', 'sila25635@gmail.com', 'a.srochno@mail.ru'];

            foreach ($senders as $sender) {
                $mailer = new Sender();
                $mailer->sendMail($sender, 'Запись на занятие', $managerContent);
            }

            if (!empty($this->app->request->post->email)) {

                $recipientContent = <<<RMSG
<h2>Вы записались на занятие: {$lessonTitle}</h2>
<p>Занятие состоится {$dateStart->format('d-m-Y')} с {$dateStart->format('H:i')} до {$dateEnd->format('H:i')}</p>
<p>Инструктор: {$teacherFirstName} {$teacherLastName}</p>
<p>{$roomTitle}</p>
RMSG;

                $recipient = new Sender();
                $recipient->sendMail($this->app->request->post->email, 'Запись на занятие', $recipientContent);
            }
            $this->data->result = true;

            if ('127.0.0.1' != $_SERVER['REMOTE_ADDR'] || '::1' != $_SERVER['REMOTE_ADDR']) {
                $roistatData = array(
                    'roistat' => isset($_COOKIE['roistat_visit']) ? $_COOKIE['roistat_visit'] : null,
                    'key'     => 'MTA1NDc6MTY4MTg6YjA0ZmY5ZTU5YjAxNTJiOTE2YjRjNjNkMmU4OTBkMTY=',
                    'title'   => 'Заявка с сайта silaclub.ru',
                    'comment' => $managerContent,
                    'name'    => $msg->name,
                    'email'   => $msg->email,
                    'phone'   => $msg->phone,
                    'is_need_callback' => '0',
                    'fields'  => array(),
                );

                file_get_contents("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData));
            }
        } else {
            $this->data->result = false;
        }
    }
}