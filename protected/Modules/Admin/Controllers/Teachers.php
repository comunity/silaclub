<?php

namespace App\Modules\Admin\Controllers;

use App\Components\Admin\Controller;
use App\Models\Teacher;

class Teachers
    extends Controller
{
    public function actionDefault()
    {
        $this->data->items = Teacher::findAll();
    }

    public function actionEdit($id = null)
    {
        if (null === $id || 'new' == $id) {
            $this->data->item = new Teacher();
        } else {
            $this->data->item = Teacher::findByPK($id);
        }
    }

    public function actionSave()
    {
        if (!empty($this->app->request->post->id)) {
            $item = Teacher::findByPK($this->app->request->post->id);
        } else {
            $item = new Teacher();
        }
        $item->fill($this->app->request->post);
        $item
            ->uploadImage('image')
            ->save();
        $this->redirect('/admin/teachers/');
    }

    public function actionDeleteImage($id)
    {
        $item = Teacher::findByPK($id);
        if ($item) {
            $item->deleteImage();
            $item->save();
            $this->data->result = true;
        } else {
            $this->data->result = false;
        }
    }

    public function actionDelete($id)
    {
        $item = Teacher::findByPK($id);
        $item->delete();
        $this->redirect('/admin/teachers/');
    }
}