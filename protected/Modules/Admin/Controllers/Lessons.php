<?php

namespace App\Modules\Admin\Controllers;

use App\Components\Admin\Controller;
use App\Models\Lesson;

class Lessons
    extends Controller
{
    public function actionDefault()
    {
        $this->data->items = Lesson::findAll(['order' => 'title']);
    }

    public function actionEdit($id = null)
    {
        if (null === $id || 'new' == $id) {
            $this->data->item = new Lesson();
        } else {
            $this->data->item = Lesson::findByPK($id);
        }
    }

    public function actionSave()
    {
        if (!empty($this->app->request->post->id)) {
            $item = Lesson::findByPK($this->app->request->post->id);
        } else {
            $item = new Lesson();
        }
        $item->fill($this->app->request->post);
        $item->save();
        $this->redirect('/admin/lessons/');
    }

    public function actionDelete($id)
    {
        $item = Lesson::findByPK($id);
        $item->delete();
        $this->redirect('/admin/lessons/');
    }
}