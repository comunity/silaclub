<?php

namespace App\Modules\Admin\Controllers;

use App\Components\Admin\Controller;
use App\Models\Entry;
use App\Models\Lesson;
use App\Models\Room;
use App\Models\Status;
use App\Models\Teacher;

class Entries
    extends Controller
{
    protected static function getEntriesByDate($date)
    {
        $entries = Entry::findAll(['where' => 'DATE(timeStart) = :date',
            'params' => [':date' => $date],
            'order' => 'timeStart ASC']);

        return $entries;
    }

    public function actionDefault($date = null)
    {
        if (empty($date)) {
            $date = date('Y-m-d');
        }

        $this->data->items = self::getEntriesByDate($date);
        $this->data->currentDate = $date;
    }

    public function actionEdit($id = null, $date = null)
    {
        if (null === $id || 'new' == $id) {
            $this->data->item = new Entry();
            $this->data->item->timeStart = $date;
            $this->data->item->timeEnd = $date;
        } else {
            $this->data->item = Entry::findByPK($id);
        }
        $this->data->teachers = Teacher::findAll();
        $this->data->lessons = Lesson::findAll(['order' => 'title']);
        $this->data->rooms = Room::findAll();
        $this->data->statuses = Status::findAll();
    }

    public function actionSave()
    {
        if (!empty($this->app->request->post->id)) {
            $item = Entry::findByPK($this->app->request->post->id);
        } else {
            $item = new Entry();
        }
        if (empty($this->app->request->post->published)) {
            $item->published = 0;
        }
        $item->fill($this->app->request->post);
        $item->save();
        $this->redirect('/admin/entries?date=' . date_format(date_create($item->timeStart), 'Y-m-d'));
    }

    public function actionDelete($id)
    {
        $item = Entry::findByPK($id);
        $item->delete();
        $this->redirect('/admin/entries?date=' . date_format(date_create($item->timeStart), 'Y-m-d'));
    }

    public function actionGetEntriesByDate($date = null)
    {
        if (empty($date)) {
            $date = date('Y-m-d');
        }

        $this->data->items = self::getEntriesByDate($date);
        $this->data->currentDate = $date;
    }

    public function actionGetEntriesToCopy($copyDate, $date)
    {
        if (empty($copyDate)) {
            $copyDate = date('Y-m-d');
        }

        $this->data->items = self::getEntriesByDate($copyDate);
        $this->data->currentDate = $date;
    }

    public function actionPrintEntries($date)
    {
        $date = new \DateTime($date);
        if (0 == $date->format('w')) {
            $monday = clone $date;
            $monday->modify('Monday previous week');
            $sunday = $date;
        } else {
            $monday = clone $date;
            $monday->modify('Monday this week');
            $sunday = clone $date;
            $sunday->modify('Sunday');
        }
        $entries = Entry::findAll(['where' => 'DATE(timeStart) BETWEEN :start AND :end AND published = 1',
                                   'params' => [':start' => $monday->format('Y-m-d'), ':end' => $sunday->format('Y-m-d')],
                                   'order' => 'timeStart ASC'
        ]);

        $week = $entries->group(function($entry) {
            return (new \DateTime($entry->timeStart))->format('Y-m-d');
        });

        $this->data->week = $week;
    }

    public function actionPublish($date)
    {
        if (!empty($this->app->request->post->items)) {
            foreach ($this->app->request->post->items as $item) {
                $entry = Entry::findByPK($item->id);
                $entry->published = !empty($item->published) ? 1 : 0;
                $entry->save();
            }
        }
        $this->redirect('/admin/entries?date=' . $date);
    }

    public function actionCopy($date)
    {
        if (!empty($this->app->request->post->items)) {
            foreach ($this->app->request->post->items as $item) {
                if (!empty($item->copy) && 1 == $item->copy) {
                    $entry = Entry::findByPK($item->id);
                    $copyEntry = clone $entry;
                    $copyEntry->timeStart = $date . ' ' . (new \DateTime($copyEntry->timeStart))->format('H:i:s');
                    $copyEntry->timeEnd = $date . ' ' . (new \DateTime($copyEntry->timeEnd))->format('H:i:s');
                    $copyEntry->save();
                }
            }
        }

        $this->redirect('/admin/entries?date=' . $date);
    }
}
