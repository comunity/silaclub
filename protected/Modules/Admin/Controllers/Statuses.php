<?php

namespace App\Modules\Admin\Controllers;

use App\Components\Admin\Controller;
use App\Models\Status;

class Statuses
    extends Controller
{
    public function actionDefault()
    {
        $this->data->items = Status::findAll();
    }

    public function actionEdit($id = null)
    {
        if (null === $id || 'new' == $id) {
            $this->data->item = new Status();
        } else {
            $this->data->item = Status::findByPK($id);
        }
    }

    public function actionSave()
    {
        if (!empty($this->app->request->post->id)) {
            $item = Status::findByPK($this->app->request->post->id);
        } else {
            $item = new Status();
        }
        $item->fill($this->app->request->post);
        $item->save();
        $this->redirect('/admin/statuses/');
    }

    public function actionDelete($id)
    {
        $item = Status::findByPK($id);
        $item->delete();
        $this->redirect('/admin/statuses/');
    }
}