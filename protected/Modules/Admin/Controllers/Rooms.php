<?php

namespace App\Modules\Admin\Controllers;

use App\Components\Admin\Controller;
use App\Models\Room;

class Rooms
    extends Controller
{
    public function actionDefault()
    {
        $this->data->items = Room::findAll();
    }

    public function actionEdit($id = null)
    {
        if (null === $id || 'new' == $id) {
            $this->data->item = new Room();
        } else {
            $this->data->item = Room::findByPK($id);
        }
    }

    public function actionSave()
    {
        if (!empty($this->app->request->post->id)) {
            $item = Room::findByPK($this->app->request->post->id);
        } else {
            $item = new Room();
        }
        $item->fill($this->app->request->post);
        $item
            ->uploadImage('image')
            ->save();
        $this->redirect('/admin/rooms/');
    }

    public function actionDeleteImage($id)
    {
        $item = Room::findByPK($id);
        if ($item) {
            $item->deleteImage();
            $item->save();
            $this->data->result = true;
        } else {
            $this->data->result = false;
        }
    }

    public function actionDelete($id)
    {
        $item = Room::findByPK($id);
        $item->delete();
        $this->redirect('/admin/rooms/');
    }
}