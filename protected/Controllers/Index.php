<?php

namespace App\Controllers;

use App\Modules\Dishes\Models\Category;
use App\Modules\Dishes\Models\Dish;
use T4\Mvc\Controller;

class Index
    extends Controller
{

    public function actionDefault()
    {
    }

    public function action404()
    {
    }

    public function actionCaptcha($config = null)
    {
        if (null !== $config) {
            $config = $this->app->config->extensions->captcha->$config;
        }
        $this->app->extensions->captcha->generateImage($config);
        die;
    }

    public function actionContacts()
    {
    }

    public function actionCafe()
    {
        $date = date('Y-m-d');
        $dishes = Dish::findAll(['where' => 'cookingdate = :date', 'params' => [':date' => $date], 'order' => '__category_id']);
        $dishes = $dishes->group('__category_id');
        $this->data->items = $dishes;
        $this->data->currentDate = $date;
    }

    public function actionAbout()
    {
    }

    public function actionPrice()
    {
    }

    public function actionPrivacy()
    {
    }

    /*public function actionPodarokangelu()
    {
    }*/
}
