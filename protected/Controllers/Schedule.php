<?php

namespace App\Controllers;

use App\Models\Entry;
use T4\Mvc\Controller;

class Schedule
    extends Controller
{
    public function actionDefault()
    {
        $now = date('Y-m-d');
        $entries = Entry::findAll(['where' => 'DATE(timeStart) = :now AND published = 1',
                                   'params' => [':now' => $now],
                                   'order' => 'timeStart ASC']);
        $this->data->items = $entries;
        $this->data->currentDate = $now;
        $this->data->maxDate = Entry::getMaxDate();
    }

    public function actionGetEntriesByDate($date = null)
    {
        if (empty($date)) {
            $date = date('Y-m-d');
        }

        $entries = Entry::findAll(['where' => 'DATE(timeStart) = :date  AND published = 1',
                                   'params' => [':date' => $date],
                                   'order' => 'timeStart ASC']);
        $this->data->items = $entries;
        $this->data->currentDate = $date;
    }

    public function actionOneEntry($id)
    {
        $this->data->item = Entry::findByPK($id);
    }
}