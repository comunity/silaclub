<?php

namespace App\Extensions\PhoneModifier;

use T4\Core\Session;

class Extension
    extends \T4\Core\Extension
{
    public function init()
    {
        if (!empty($this->app->request->get->social) && '1' == $this->app->request->get->social) {
            Session::set('social', true);
        }

        if (Session::get('social')) {
            $this->app->config->phone = $this->options->replacement;
        }
    }
}