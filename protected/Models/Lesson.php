<?php

namespace App\Models;

use T4\Orm\Model;

class Lesson
    extends Model
{
    protected static $schema = [
        'table' => 'lessons',
        'columns' => [
            'title' => ['type' => 'string'],
            'desc' => ['type' => 'text'],
        ],
        'relations' => [

        ],
    ];
}