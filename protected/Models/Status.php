<?php

namespace App\Models;

use T4\Orm\Model;

class Status
    extends Model
{
    protected static $schema = [
        'table' => 'statuses',
        'columns' => [
            'title' => ['type' => 'string'],
            'desc' => ['type' => 'text'],
        ],
        'relations' => [

        ]
    ];
}