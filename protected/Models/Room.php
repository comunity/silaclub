<?php

namespace App\Models;

use T4\Orm\Model;

class Room
    extends Model
{
    protected static $schema = [
        'table' => 'rooms',
        'columns' => [
            'title' => ['type' => 'string'],
            'desc' => ['type' => 'text'],
            'image' => ['type' => 'string'],
        ],
        'relations' => [

        ],
    ];

    public function uploadImage($formFieldName)
    {
        /*$request = Application::getInstance()->request;
        if (!$request->existsFilesData() || !$request->isUploaded($formFieldName) || $request->isUploadedArray($formFieldName))
            return $this;

        try {
            $uploader = new Uploader($formFieldName);
            $uploader->setPath('/public/news/stories/images');
            $image = $uploader();
            if ($this->image)
                $this->deleteImage();
            $this->image = $image;
        } catch (Exception $e) {
            $this->image = null;
        }*/
        return $this;
    }

    public function deleteImage()
    {
        /*if ($this->image) {
            try {
                Helpers::removeFile(ROOT_PATH_PUBLIC . $this->image);
                $this->image = '';
            } catch (\T4\Fs\Exception $e) {
                return false;
            }
        }*/
        return true;
    }


}