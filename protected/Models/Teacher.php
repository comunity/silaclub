<?php

namespace App\Models;

use T4\Orm\Model;

class Teacher
    extends Model
{
    protected static $schema = [
        'table' => 'teachers',
        'columns' => [
            'firstName' => ['type' => 'string', 'length' => 50],
            'lastName' => ['type' => 'string', 'length' => 50],
            'patronymic' => ['type' => 'string', 'lenght' => 50],
            'email' => ['type' => 'string'],
            'desc' => ['type' => 'text'],
            'photo' => ['type' => 'string'],
        ],
        'relations' => [

        ],
    ];

    public function uploadImage($formFieldName)
    {
        /*$request = Application::getInstance()->request;
        if (!$request->existsFilesData() || !$request->isUploaded($formFieldName) || $request->isUploadedArray($formFieldName))
            return $this;

        try {
            $uploader = new Uploader($formFieldName);
            $uploader->setPath('/public/news/stories/images');
            $image = $uploader();
            if ($this->image)
                $this->deleteImage();
            $this->image = $image;
        } catch (Exception $e) {
            $this->image = null;
        }*/
        return $this;
    }

    public function deleteImage()
    {
        /*if ($this->image) {
            try {
                Helpers::removeFile(ROOT_PATH_PUBLIC . $this->image);
                $this->image = '';
            } catch (\T4\Fs\Exception $e) {
                return false;
            }
        }*/
        return true;
    }


}