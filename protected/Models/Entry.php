<?php

namespace App\Models;

use T4\Dbal\QueryBuilder;
use T4\Orm\Model;

class Entry
    extends Model
{
    protected static $schema = [
        'table' => 'entries',
        'columns' => [
            'timeStart' => ['type' => 'datetime'],
            'timeEnd' => ['type' => 'datetime'],
            'comment' => ['type' => 'text'],
            'published' => ['type' => 'bool', 'default' => 0],
        ],
        'relations' => [
            'lesson' => ['type' => self::BELONGS_TO, 'model' => Lesson::class, 'order' => 'title'],
            'teacher' => ['type' => self::BELONGS_TO, 'model' => Teacher::class],
            'room' => ['type' => self::BELONGS_TO, 'model' => Room::class],
            'status' => ['type' => self::BELONGS_TO, 'model' => Status::class],
        ],
    ];

    public static function getMaxDate()
    {
        $query = (new QueryBuilder())
            ->select('max(timeEnd)')
            ->where('published = 1')
            ->from(self::getTableName());
        $maxDate = self::getDbConnection()->query($query)->fetchScalar();
        $maxDate = (new \DateTime($maxDate))->format('Y-m-d');
        return $maxDate;
    }

    public function __clone()
    {
        $this->__id = null;
        $this->published = 0;
        $this->isNew = true;
    }
}