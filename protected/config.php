<?php

return [
    'db' => [
        'default' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'dbname' => 'sila',
            'user' => 'sila',
            'password' => 'A6v8M5m1',
        ]
    ],
    'auth' => [
        'expire' => 31536000 // 1 year
    ],
    'mail' => [
        'method' => 'smtp',
        'host' => 'smtp.yandex.ru',
        'auth' => [
            'username' => 'sale@silaclub.ru',
            'password' => '0L5a5I5s'
        ],
        'sender' => 'SILA',
        'port' => '587',
        'secure' => 'tls',
    ],
    'extensions' => [
        'jquery' => [
        ],
        'bootstrap' => [
            'location' => 'local',
            'theme' => 'cerulean',
        ],
        'ckeditor' => [
            'location' => 'local',
        ],
        'ckfinder' => [
            'autoload' => false,
        ],
        'jstree' => [
            'autoload' => false,
        ],
        'captcha' => [
            'register' => true,
            'message' => true,
        ],
        /*'phonemodifier' => [
            'replacement' => '+7 (495)191-36-03',
        ],*/
    ],
    'errors' => [
        404 => '///404',
    ],
    'phone' => '+7 (495)104-68-88',
];