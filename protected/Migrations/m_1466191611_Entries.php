<?php

namespace App\Migrations;

use T4\Orm\Migration;

class m_1466191611_Entries
    extends Migration
{

    public function up()
    {
        $this->createTable('entries', [
            'timeStart' => ['type' => 'datetime'],
            'timeEnd' => ['type' => 'datetime'],
            'comment' => ['type' => 'text'],
            '__lesson_id' => ['type' => 'link'],
            '__teacher_id' => ['type' => 'link'],
            '__room_id' => ['type' => 'link'],
        ]);
    }

    public function down()
    {
        $this->dropTable('entries');
    }

}