<?php

namespace App\Migrations;

use T4\Orm\Migration;

class m_1471508651_Statuses
    extends Migration
{

    public function up()
    {
        $this->createTable('statuses', [
            'title' => ['type' => 'string'],
            'desc' => ['type' => 'text'],
        ]);
    }

    public function down()
    {
        $this->dropTable('statuses');
    }

}