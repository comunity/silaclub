<?php

namespace App\Migrations;

use T4\Orm\Migration;

class m_1466187285_Lessons
    extends Migration
{

    public function up()
    {
        $this->createTable('lessons', [
            'title' => ['type' => 'string'],
            'desc' => ['type' => 'text'],
        ]);
    }

    public function down()
    {
        $this->dropTable('lessons');
    }

}