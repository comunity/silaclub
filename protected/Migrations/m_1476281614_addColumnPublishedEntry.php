<?php

namespace App\Migrations;

use T4\Orm\Migration;

class m_1476281614_addColumnPublishedEntry
    extends Migration
{

    public function up()
    {
        $this->addColumn('entries', [
            'published' => ['type' => 'bool', 'default' => 0],
        ]);
    }

    public function down()
    {
        $this->dropColumn('entries', ['published']);
    }

}