<?php

namespace App\Migrations;

use T4\Orm\Migration;

class m_1473259478_practicesToTrainers
    extends Migration
{

    public function up()
    {
        $this->createTable('practices_to_trainers', [
            '__practice_id' => ['type' => 'link'],
            '__trainer_id' => ['type' => 'link'],
        ]);
    }

    public function down()
    {
        $this->dropTable('practices_to_trainers');
    }

}