<?php

namespace App\Migrations;

use T4\Orm\Migration;

class m_1476339400_delByphoneColumn
    extends Migration
{

    public function up()
    {
        $this->dropColumn('entries', ['byphone']);
    }

    public function down()
    {
        $this->addColumn('entries', [
            'byphone' => ['type' => 'bool'],
        ]);
    }

}