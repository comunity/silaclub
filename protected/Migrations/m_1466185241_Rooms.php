<?php

namespace App\Migrations;

use T4\Orm\Migration;

class m_1466185241_Rooms
    extends Migration
{

    public function up()
    {
        $this->createTable('rooms', [
            'title' => ['type' => 'string'],
            'desc' => ['type' => 'text'],
            'image' => ['type' => 'string'],
        ]);
    }

    public function down()
    {
        $this->dropTable('rooms');
    }

}