<?php

namespace App\Migrations;

use T4\Orm\Migration;

class m_1466172120_Teachers
    extends Migration
{

    public function up()
    {
        $this->createTable('teachers', [
            'firstName' => ['type' => 'string', 'length' => 50],
            'lastName' => ['type' => 'string', 'length' => 50],
            'patronymic' => ['type' => 'string', 'lenght' => 50],
            'email' => ['type' => 'string'],
            'desc' => ['type' => 'text'],
            'photo' => ['type' => 'string'],
        ]);
    }

    public function down()
    {
        $this->dropTable('teachers');
    }

}