<?php

namespace App\Migrations;

use T4\Orm\Migration;

class m_1471509152_addColumnEntries
    extends Migration
{

    public function up()
    {
        $this->addColumn('entries', [
            '__status_id' => ['type' => 'link'],
            'byphone' => ['type' => 'bool'],
        ]);
    }

    public function down()
    {
        $this->dropColumn('entries', ['__status_id', 'byphone']);
    }

}