<?php

return [

    '/admin/blocks'         => '/Admin/Blocks//',
    '/admin/blocks/<1>'     => '/Admin/Blocks/<1>/',
    '/admin/entries'          => '/Admin/entries//',
    '/admin/entries/<1>'      => '/Admin/entries/<1>/',
    '/admin/rooms'          => '/Admin/Rooms//',
    '/admin/rooms/<1>'      => '/Admin/Rooms/<1>/',
    '/admin/lessons'          => '/Admin/lessons//',
    '/admin/lessons/<1>'      => '/Admin/lessons/<1>/',
    '/admin/teachers'       => '/Admin/Teachers//',
    '/admin/teachers/<1>'   => '/Admin/Teachers/<1>/',
    '/admin/statuses'       => '/Admin/Statuses//',
    '/admin/statuses/<1>'   => '/Admin/Statuses/<1>/',
    '/admin/<1>/<2>/<3>'    => '/Admin//Module(module=<1>,controller=<2>,action=<3>)',
    '/admin/<1>/<2>'        => '/Admin//Module(module=<1>,action=<2>)',
    '/admin/<1>'            => '/Admin//Module(module=<1>)',
    '/admin/promo'          => '/Admin/Promo//',
    '/admin/partners'       => '/Admin/Partners//',

    '/news/<1>' => '/News//One(id=<1>)',

    '/index' => '///',
    '/pages/<1>/<2>'    => '/Pages/Index/PageByUrl(url=<2>)',
    '/pages/<1>'        => '/Pages/Index/PageByUrl(url=<1>)',
    '/practices/<1>'    => '/Practices/Index/PracticeByUrl(url=<1>)',
    '/trainers/<1>'     => '/Trainers/Index/TrainerByUrl(url=<1>)',
    '/tour'             => '/Tour//',
    '/promo'            => '/Promo//',
    '/events/<1>'       => '/Events/Index/EventByUrl(url=<1>)',
];