jQuery(document).ready(function(){
        
    $(window).scroll(function() {
        if($(this).scrollTop() < 100) {
            $('#fix_footer').fadeOut(100);
        } else {
            $('#fix_footer').fadeIn(100);
        }
    });

    // STICKY NAV
    function StickyNav(elem) {
        var element = elem;
        var elemPos = element.offset().top;
        $(window).scroll(function(){
            var windowPos = $(window).scrollTop();

            if(windowPos >= elemPos) {
                element.parent('.header').addClass('fixed-top');
            }else {
                element.parent('.header').removeClass('fixed-top');
            }
        });
    }

    StickyNav($('.top_fixed'));

    //PopUp
    $('a.show_popup').click(function () {
        $('div.'+$(this).attr("rel")).fadeIn(500);
        $("body").css({'overflow':'hidden'});
        $('#overlay').show().css({'filter' : 'alpha(opacity=80)'});
        return false;               
    }); 
        $('a.close').click(function () {
        $(this).parent().fadeOut(100);
        $("body").css({'overflow':'auto'});
        return false;
    });


    /* Плавный скрол к блоку */
    $('a.anchor').click(function(){
        var idscroll = $(this).attr('href');
        $.scrollTo(idscroll, 500, {offset: -100});
        return false;
    });


});