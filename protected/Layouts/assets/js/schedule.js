$(function() {
    function getEntriesByDate(date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var year = date.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        $.get('/schedule/GetEntriesByDate.html', { date: year + '-' + mm + '-' + dd}, function(data) {
            $('#entries').html('').append(data);
        }, "html");
    }

    var nowDate = new Date();
    var currDate = new Date();
    var maxDate = new Date($('#next-date').data('maxDate'));
    var days = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
    var monthNames = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

    $('#next-date, #previous-date, #today').on('click', function(e) {
        if (e.target.id == 'next-date') {
            currDate.setDate(currDate.getDate() + 1);
        } else if (e.target.id == 'previous-date') {
            if (currDate <= nowDate) {
                $(this).addClass('disable');
                return false;
            }
            currDate.setDate(currDate.getDate() - 1);
        } else if (e.target.id == 'today') {
            currDate = new Date($('#today').data('currDate'));
        }

        getEntriesByDate(currDate);

        if (currDate <= nowDate) {
            $('#previous-date').addClass('disable');
        } else if($('#previous-date').hasClass('disable')) {
            $('#previous-date').removeClass('disable');
        }

        if (currDate >= maxDate) {
            $('#next-date').addClass('disable');
        } else if($('#next-date').hasClass('disable')) {
            $('#next-date').removeClass('disable');
        }
    });

    $( "#datepicker" ).datepicker({
        minDate: nowDate,
        maxDate: maxDate,
        dateFormat: 'yy-mm-dd',
        dayNamesMin: days,
        monthNames: monthNames,
        firstDay: 1,
        onSelect: function(e) {
            currDate = new Date($(this).val());
            getEntriesByDate(currDate);
            if (currDate <= nowDate) {
                $('#previous-date').addClass('disable');
            } else if($('#previous-date').hasClass('disable')) {
                $('#previous-date').removeClass('disable');
            }
            if (currDate >= maxDate) {
                $('#next-date').addClass('disable');
            } else if($('#next-date').hasClass('disable')) {
                $('#next-date').removeClass('disable');
            }
        }
    });

    $('#callbackModal').on('show.bs.modal', function (event) {
        if ( $(event.relatedTarget).data() ) {
            var button = $(event.relatedTarget);
            var title = button.data('title');
            var date = button.data('date');
            var time = button.data('time');
            var entryPk = button.data('entryPk');
            var modal = $(this);
            modal.find('.modal-title').text(title);
            modal.find('.modal-title-date').text(date);
            modal.find('.modal-title-time').text(time);
            modal.find('#entry').val(entryPk);
        }
    });

    if (!(navigator.userAgent.toUpperCase().indexOf("WINDOWS") > -1)) {
        $('#phone').mask("+7(999) 999-99-99");
    }

    $("#entry-contact").submit(function(event) {
        event.preventDefault();
        $('#callbackModal').modal('hide');

        $.post('/contact/send.json', $(this).serialize(), function(data) {
            if (typeof(data.errors) != "undefined" && data.errors.length) {
                var errors = '';
                $.each(data.errors, function(key, val) {
                    errors += "<p class='text-danger'>" + val + "</p>";
                });
                $('#errorsModal .modal-body').html(errors);
                $('#errorsModal').modal('show');
            } else if(data.result) {
                $('#callbackDoneModal').modal('show');
            } else {
                alert("Извините, произошла ошибка на сервере.\nПовторите попытку позже.\nИли запишитесь по телефону: " + data.phone);
            }
        }).fail(function() {
            alert("Извините, произошла ошибка на сервере.\nПовторите попытку позже.");
        });
    });

    $('#errorsModal').on('hide.bs.modal', function (event) {
        $('#callbackModal').modal('show');
    });

});